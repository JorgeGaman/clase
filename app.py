# calculadora suma, resta, multiplicacion y division en terminal

import os


def clear():
    os.system("cls")


def suma(num1, num2):
    return num1 + num2


def resta(num1, num2):
    return num1 - num2


def multiplicacion(num1, num2):
    return num1 * num2


def division(num1, num2):
    return num1 / num2


def pedirNumero():
    while True:
        try:
            num = int(input("Introduce un numero: "))
            break
        except ValueError:
            print("No es un numero. Intentalo de nuevo")
    return num


def pedirOperacion():
    while True:
        try:
            operacion = int(input("Introduce una operacion: "))
            if operacion > 0 and operacion < 5:
                break
            else:
                print("No es una operacion valida. Intentalo de nuevo")
        except ValueError:
            print("No es una operacion valida. Intentalo de nuevo")
    return operacion


def pedirContinuar():
    while True:
        try:
            continuar = int(input("¿Quieres continuar? 1-Si 2-No: "))
            if continuar > 0 and continuar < 3:
                break
            else:
                print("No es una opcion valida. Intentalo de nuevo")
        except ValueError:
            print("No es una opcion valida. Intentalo de nuevo")
    return continuar


def mostrarResultado(num1, num2, operacion, resultado):
    if operacion == 1:
        print(f"El resultado de la suma de {num1} y {num2} es {resultado}")
    elif operacion == 2:
        print(f"El resultado de la resta de {num1} y {num2} es {resultado}")
    elif operacion == 3:
        print(
            f"El resultado de la multiplicacion de {num1} y {num2} es {resultado}")
    elif operacion == 4:
        print(f"El resultado de la division de {num1} y {num2} es {resultado}")


def mostrarMenu():
    print("1-Suma")
    print("2-Resta")
    print("3-Multiplicacion")
    print("4-Division")


def pedirDatos():
    num1 = pedirNumero()
    num2 = pedirNumero()
    operacion = pedirOperacion()
    return num1, num2, operacion


def calcular(num1, num2, operacion):
    if operacion == 1:
        resultado = suma(num1, num2)
    elif operacion == 2:
        resultado = resta(num1, num2)
    elif operacion == 3:
        resultado = multiplicacion(num1, num2)
    elif operacion == 4:
        resultado = division(num1, num2)
    return resultado


def continuar():
    continuar = pedirContinuar()
    if continuar == 1:
        return True
    else:
        return False


def main():
    while True:
        clear()
        mostrarMenu()
        num1, num2, operacion = pedirDatos()
        resultado = calcular(num1, num2, operacion)
        mostrarResultado(num1, num2, operacion, resultado)
        if not continuar():
            break


main()
